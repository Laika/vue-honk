import Vue from 'vue'
import App from './App.vue'

Vue.directive('focus', {
  inserted: function(el) {
    setTimeout(function() {
      el.focus();
    }, 200);
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
})
