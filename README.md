# Language learning tool

> A quite simple Vue.js front for my language learning API

This is a simple app that functions as a tool to help people memorizing words
when studying a foreign language, in my case at the moment Korean. As it stands it fetches 
data from a REST API created with Python/Flask and allows you to go over user added 
courses/sections.

For now though there is no admin panel that would allow just anyone to add courses,
so you are stuck with whatever I have added myself and that's Korean.

A running example of this can be found at: [honk.fr/korean](https://test.honk.fr/korean)<br/>
The REST API's source is over [HERE](https://gitlab.com/Laika/honk-api)


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
